<?php
/**
 * Created by PhpStorm.
 * User: dongyanan
 * Date: 2019/7/4
 * Time: 7:17 PM
 */

namespace Ufile\Lumen\Ucloud;


class UcloudMCancel extends Ucloud
{

    public function __construct($bucket, $key, $uploadId)
    {
        $this->bucket = $bucket;
        $this->key = $key;
        $this->path = $key;

        $this->query = ['uploadId' => $uploadId];

        $this->setHost($bucket);
        $this->setMimeType('application/x-www-form-urlencoded');

    }

    public function clientCall($req)
    {
        if ($this->errNo) {

            $http = new Http();

            list($resp, $err) = $http->UCloud_Client_Do($req);

            if ($err !== null) {
                return array(
                    'code' => $err->Code,
                    'msg' => $err->ErrMsg
                );
            }

            list($data, $errClient) = $http->UCloud_Client_Ret($resp);

            if ($errClient !== null) {
                return array(
                    'code' => $err->Code,
                    'msg' => $err->ErrMsg
                );
            }

            return array(
                'code' => '200',
                'msg' => $data['ETag']
            );

        } else {

            return array(
                'code' => '702',
                'msg' => 'something is wrong'
            );

        }
    }
}