<?php
/**
 * Created by PhpStorm.
 * User: dongyanan
 * Date: 2019/7/4
 * Time: 7:17 PM
 */

namespace Ufile\Lumen\Ucloud;


class UcloudMFinish extends Ucloud
{

    public function __construct($bucket, $key, $uploadId, $etagList, $newKey = '')
    {
        $this->bucket = $bucket;
        $this->path = $key;
        $this->query = ['uploadId' => $uploadId,'newKey' => $newKey,];
        $this->key = $key;
        $this->content = implode(',',$etagList);

        $this->setHost($bucket);
        $this->setMimeType('text/plain');
    }

    public function clientCall($req)
    {
        if ($this->errNo) {

            $http = new Http();

            list($resp, $err) = $http->UCloud_Client_Do($req);

            if ($err !== null) {
                return array(
                    'code' => $err->Code,
                    'msg' => $err->ErrMsg
                );
            }

            list($data, $errClient) = $http->UCloud_Client_Ret($resp);

            if ($errClient !== null) {
                return array(
                    'code' => $err->Code,
                    'msg' => $err->ErrMsg
                );
            }

            return array(
                'code' => '200',
                'msg' => $data['ETag']
            );

        } else {

            return array(
                'code' => '702',
                'msg' => 'something is wrong'
            );

        }
    }
}