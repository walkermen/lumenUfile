<?php
/**
 * Created by PhpStorm.
 * User: dongyanan
 * Date: 2019/7/4
 * Time: 10:06 AM
 */

namespace Ufile\Lumen\Ucloud;


class UcloudDeleteFile extends Ucloud
{
    public function __construct($bucket, $key, $mimeType = 'application/x-www-form-urlencoded')
    {
        $this->bucket = $bucket;
        $this->path = $key;

        $this->checkConfig(Ucloud::DELETE);
        $this->setHost($bucket);

        $this->setMimeType($mimeType);
    }

    public function clientCall($req)
    {
        if ($this->errNo) {

            $http = new Http();

            list($resp, $err) = $http->UCloud_Client_Do($req);

            if ($err !== null) {
                return array(
                    'code' => $err->Code,
                    'msg' => $err->ErrMsg
                );
            }

            list($data, $errClient) = $http->UCloud_Client_Ret($resp);

            if ($errClient !== null) {
                return array(
                    'code' => $err->Code,
                    'msg' => $err->ErrMsg
                );
            }

            return array(
                'code' => '200',
                'msg' => $data['ETag']
            );

        } else {

            return array(
                'code' => '702',
                'msg' => 'something is wrong'
            );

        }
    }
}