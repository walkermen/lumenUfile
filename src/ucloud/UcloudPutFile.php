<?php
/**
 * Created by PhpStorm.
 * User: dongyanan
 * Date: 2019/7/2
 * Time: 5:14 PM
 */

namespace Ufile\Lumen\Ucloud;


class UcloudPutFile extends Ucloud
{
    public function __construct($bucket, $key, $file, $actionType = Ucloud::PUTFILE)
    {
        $this->path = $key;
        $this->file = $file;
        $this->bucket = $bucket;
        $this->key = $key;

        $this->setHost($bucket);
        $this->checkConfig($actionType);
        $this->getFileMimeType();
        $this->readFileContent();
    }

    public function clientCall($req)
    {
        if ($this->errNo) {

            $http = new Http();

            list($resp, $err) = $http->UCloud_Client_Do($req);

            if ($err !== null) {
                return array(
                    'code' => $err->Code,
                    'msg' => $err->ErrMsg
                );
            }

            list($data, $errClient) = $http->UCloud_Client_Ret($resp);

            if ($errClient !== null) {
                return array(
                    'code' => $err->Code,
                    'msg' => $err->ErrMsg
                );
            }

            return array(
                'code' => '200',
                'msg' => $data['ETag']
            );

        } else {

            return array(
                'code' => '702',
                'msg' => 'something is wrong'
            );

        }
    }

}