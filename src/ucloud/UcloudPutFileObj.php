<?php

namespace Ufile\Lumen\Ucloud;

use Illuminate\Http\UploadedFile;

class UcloudPutFileObj extends Ucloud
{
    public function __construct($bucket, $key, UploadedFile $file, $actionType = Ucloud::PUTFILE)
    {
        $this->path = $key;
        $this->bucket = $bucket;
        $this->key = $key;

        $this->setHost($bucket);
        $this->checkConfig($actionType);
        $this->mimeType = $file->getMimeType();
        $this->content = $file->get();
    }

    public function clientCall($req)
    {
        if ($this->errNo) {

            $http = new Http();

            list($resp, $err) = $http->UCloud_Client_Do($req);

            if ($err !== null) {
                return [
                    'code' => $err->Code,
                    'msg'  => $err->ErrMsg,
                ];
            }

            list($data, $errClient) = $http->UCloud_Client_Ret($resp);

            if ($errClient !== null) {
                return [
                    'code' => $err->Code,
                    'msg'  => $err->ErrMsg,
                ];
            }

            return [
                'code' => '200',
                'msg'  => $data['ETag'],
                'data' => [
                    'filename' => $this->key,
                    'mimeType' => $this->mimeType,
                    'url'      => 'http://' . config('ufile.bucket') . config('ufile.UCLOUD_PROXY_SUFFIX') . '/' . $this->key,
                ],
            ];

        } else {

            return [
                'code' => '702',
                'msg'  => 'something is wrong',
            ];

        }
    }

}
