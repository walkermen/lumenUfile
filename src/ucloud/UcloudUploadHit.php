<?php
/**
 * Created by PhpStorm.
 * User: dongyanan
 * Date: 2019/7/4
 * Time: 3:33 PM
 */

namespace Ufile\Lumen\Ucloud;


class UcloudUploadHit extends Ucloud
{

    public function __construct($bucket, $key, $file, $actionType = Ucloud::UPLOADHIT)
    {
        $this->path = 'uploadhit';
        $this->bucket = $bucket;
        $this->file = $file;

        $this->key = $key;

        $this->setHost($bucket);
        $this->checkConfig($actionType);

        $this->setQuery($key,$file);

    }

    public function clientCall($req)
    {
        if ($this->errNo) {

            $http = new Http();

            list($resp, $err) = $http->UCloud_Client_Do($req);

            if ($err !== null) {
                return array(
                    'code' => $err->Code,
                    'msg' => $err->ErrMsg
                );
            }

            list($data, $errClient) = $http->UCloud_Client_Ret($resp);

            if ($errClient !== null) {
                return array(
                    'code' => $err->Code,
                    'msg' => $err->ErrMsg
                );
            }

            return array(
                'code' => '200',
                'msg' => $data['ETag']
            );

        } else {

            return array(
                'code' => '702',
                'msg' => 'something is wrong'
            );

        }
    }

    protected function fileHash($file)
    {
        $f = fopen($file, "r");
        if (!$f) $this->err[] = new UcloudError(400, -1, "file open failed");

        $fileSize = filesize($file);
        $buffer   = '';
        $sha      = '';
        $blkcnt   = $fileSize/self::BLKSIZE;
        if ($fileSize % self::BLKSIZE) $blkcnt += 1;
        $buffer .= pack("L", $blkcnt);
        if ($fileSize <= self::BLKSIZE) {
            $content = fread($f, self::BLKSIZE);
            if (!$content) {
                fclose($f);
                $this->err[] = new UcloudError(400, -1, "read file error");
            }
            $sha .= sha1($content, TRUE);
        } else {
            for($i=0; $i<$blkcnt; $i+=1) {
                $content = fread($f, self::BLKSIZE);
                if (!$content) {
                    if (feof($f)) break;
                    fclose($f);
                    return array("", new UcloudError(0, -1, "read file error"));
                }
                $sha .= sha1($content, TRUE);
            }
            $sha = sha1($sha, TRUE);
        }
        $buffer .= $sha;
        $hash = $this->urlSafeEncode(base64_encode($buffer));
        fclose($f);
        return $hash;
    }

    protected function urlSafeEncode($data)
    {
        $find = array('+', '/');
        $replace = array('-', '_');
        return str_replace($find, $replace, $data);
    }

    protected function setQuery($key,$file)
    {
        $fileHash = $this->fileHash($file);

        $fileSize = filesize($file);

        $querys = array(
            'Hash' => $fileHash,
            'FileName' => $key,
            'FileSize' => $fileSize
        );

        $this->query = $querys;

    }
}