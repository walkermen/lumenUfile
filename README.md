# laravelUfile

#### 项目介绍
lumen 云上传

#### 软件架构
lumen 5.5 +


#### 安装教程

1. composer require ufile/ufile-lumen:dev-master

2. composer dump-autoload 
   

#### 使用说明

1. //执行上传

   第一步 实例化
   
    $ufile = new UcloudPutFile($bucket, $key, $filename);
    
   第二步 设置超时时间
   
    $ufile->setUpTimeOut(20);  //可选 默认10秒
    
   第三步 获取request对象
   
    $req = $ufile->getHttpRequestObj('PUT');
    
   第四步 判断是否有错误
   
    $ufile->checkErr('UcloudPutFile');
    
   第五步 发起请求
   
    $result = $ufile->clientCall($req);

2. //下载操作

   第一步 实例化
     
     $ufile = new UcloudGetFile($bucket, $key, $type);
     
   第二步 获取文件内容
   
     $ufile->getFile();
     
3. //删除操作

   第一步 实例化
   
     $ufile = new UcloudDeleteFile($bucket, $key); 
   
   第二步 设置超时时间
      
     $ufile->setUpTimeOut(20);  //可选 默认10秒  
     
   第三步 获取request对象
   
     $req = $ufile->getHttpRequestObj('DELETE');       
   
   第四步 判断是否有错误

     $ufile->checkErr('UcloudDeleteFile');
     
   第五步 发起请求
   
     $result = $ufile->clientCall($req);
     
4. //表单上传

   第一步 实例化
   
     $ufile = new UcloudMultipartForm($bucket, $key, $file); 
     
   第二步 设置超时时间
      
     $ufile->setUpTimeOut(20);  //可选 默认10秒
     
   第三步 发起请求
   
     $result = $ufile->clientCall();  
     
5. //秒传

   第一步 实例化
   
     $ufile = new UcloudUploadHit($bucket, $key, $file);
     
   第二步 设置超时时间及mimetype
      
     $ufile->setUpTimeOut(20);  //可选 默认10秒
     $ufile->setMimeType('application/x-www-form-urlencoded');
     
   第三步 获取request对象
   
     $req = $ufile->getHttpRequestObj('POST');
     
   第四步 判断是否有错误

     $ufile->checkErr('UcloudUploadHit');
     
   第五步 发起请求
   
     $result = $ufile->clientCall($req);                 
            
6. //分片上传

   第一步 分片初始化  获取 UploadId BlkSize
   
     $ufile = new UcloudMInit($bucket, $key);
     
     $ufile->setUpTimeOut(20);  //可选 默认10秒
     
     $req = $ufile->getHttpRequestObj('POST');
     
     $ufile->checkErr('UcloudMInit');
     
     $resule = $ufile->MInit($req);
     
   第二步 文件上传  获取上传结果 $etagList
   
     $ufile = new UcloudMUpload($bucket, $key, $file, $uploadId, $blkSize);
     
     $ufile->setUpTimeOut(20);  //可选 默认10秒
     
     list($etagList,$err) = $ufile->MUpload();
     
   第三步 根据上传结果判断是执行 分片删除或分片完成
   
     $ufile = new UcloudMFinish($bucket, $key, $uploadId, $etagList);  
     
     $ufile->setUpTimeOut(20);  //可选 默认10秒
     
     $req = $ufile->getHttpRequestObj('POST');
     
     $ufile->checkErr('UcloudMFinish');
     
     $resule = $ufile->clientCall($req); 
     
     --------------------------------------------------------
     
     $ufile = new UcloudMCancel($bucket, $key, $uploadId);         
     
     $ufile->setUpTimeOut(20);  //可选 默认10秒
     
     $req = $ufile->getHttpRequestObj('DELETE');
     
     $ufile->checkErr('UcloudMCancel');
     
     $resule = $ufile->clientCall($req);      


